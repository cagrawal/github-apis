const db = require("../models");
const GithubUser = db.githubusers;
const constants = require('../constants');
const GithubService = require('../services/github.service');

// Retrieve all repos from the github.
exports.getUserRepos = async (req, res) => {
  const { handle } = req.query;
  const url = `${constants.BASE_URL}${handle}/repos`;
  try {
    const response = await GithubService.apiCallService(url);
    return res.send(response);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while retrieving repos."
    });
  }
};

// Retrieve all repos from the github.
exports.getUserInfo = async (req, res) => {
  const { handle } = req.query;
  const url = `${constants.BASE_URL}${handle}`;

  try {
    const data = await GithubUser.findAll({ where: { handle } });
    if (data.length) {
      res.send(data[0]);
    } else {
      const response = await GithubService.apiCallService(url);
      if (response) {
        // Create a User
        const userInfo = GithubService.mapResponse(response, handle);

        // Save User in the database
        const data = await GithubUser.create(userInfo);
        res.send(data);
      } else {
        return res.send(null);
      }
    }
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while retrieving user info."
    });
  }
};
